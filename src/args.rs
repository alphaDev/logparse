use std::string::String;
use clap::Parser;

#[derive(Parser)]
#[clap(about, version, author)]
pub struct Args {
    #[clap(help = "path to the file to parse")]
    pub(crate) log_to_parse: String,

    #[clap(short = 'L', long, help = "line definition")]
    pub(crate) line_definition: Option<String>,

    #[clap(short, long, help = "file definition")]
    pub(crate) file_definition: Option<String>,

    #[clap(short, long, help = "pattern in line")]
    pub(crate) grep_pattern: Option<String>,

    #[clap(short = 'H', long, help = "header")]
    pub(crate) header: Option<String>,

    #[clap(short, long, help = "path to write the file")]
    pub(crate) output_file: Option<String>,

    #[clap(short, long, help = "name")]
    pub(crate) name: Option<String>,

    #[clap(short, long, help = "show 1 line with position")]
    pub(crate) prepare: bool,

    #[clap(short = 'P', long, help = "nb line to parse when prepare")]
    pub(crate) prepare_count: Option<usize>,

    #[clap(short = 'G', long, help = "type of graph to build : line/grouped_bar")]
    pub(crate) graph_mode: Option<String>,
}
