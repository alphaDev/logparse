use substring::Substring;

pub struct Formatter {
    pub format_type: String,
    pub from: String,
    pub to: String,
    pub start: i32,
    pub end: i32,
}

impl Formatter {
    pub fn new(raw_formatter: &str) -> Formatter {
        let (format_type, param_part) = raw_formatter.split_once(":").unwrap();

        return if format_type == "r" {
            let mut param_part_split = param_part.split("->");
            let from = param_part_split.next().unwrap_or("");
            let to = param_part_split.next().unwrap_or("");
            Formatter { format_type: String::from(format_type), from: String::from(from), to: String::from(to), start: 0, end: 0 }
        } else if format_type == "s" {
            let mut param_part_split = param_part.split("..");
            let start = param_part_split.next().unwrap().parse().unwrap_or(0);
            let end = param_part_split.next().unwrap().parse().unwrap_or(0);

            let to_print = format!("format_type 〖{}〗 start 〖{}〗 end 〖{}〗", format_type, start, end);
            println!(" ┗━➤ format_type[{}]", to_print);

            Formatter { format_type: String::from(format_type), from: String::from(""), to: String::from(""), start, end }
        } else {
            Formatter { format_type: String::from(format_type), from: String::from(""), to: String::from(""), start: 0, end: 0 }
        };
    }

    pub fn format(&self, value: String) -> String {
        if self.format_type == "r" {
            return value.replace(self.from.as_str(), self.to.as_str());
        }
        if self.format_type == "s" {
            return String::from(value.substring(self.start as usize, self.end as usize));
        }
        return value;
    }

    pub fn print(&self) {
        let to_print = format!("format_type 〖{}〗 from 〖{}〗 to 〖{}〗", self.format_type, self.from, self.to);
        println!(" ┗━➤ Formatter[{}]", to_print);
    }
}