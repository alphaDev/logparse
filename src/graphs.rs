extern crate plotly;


use std::collections::HashMap;
use std::path::Path;

use plotly::{Layout, NamedColor, Plot, Scatter};
use plotly::common::{Line, Marker, Mode};
use plotly::layout::{Shape, ShapeLine, ShapeType};

use crate::stats::Stat;

use self::plotly::Bar;
use self::plotly::common::Title;
use self::plotly::layout::BarMode;

pub fn make_graphs(stats: &Vec<Stat>) {
    for stat in stats {
        make_one_stat_graph(stat, stats);
    }
}

pub fn make_one_stat_graph(stat: &Stat, stats: &Vec<Stat>) {
    let stat_name = &stat.name;
    let mut layout: Layout = if stat.graph_mode == "grouped_bar" { Layout::new().title(Title::from(stat_name.as_str())).bar_mode(BarMode::Group) } else { Layout::new().title(Title::from(stat_name.as_str())) };
    let mut plot = Plot::new();

    let folder_path = Path::new(stat.output_folder.as_str());
    let graph_file_path = folder_path.join(format!("{}{}", stat_name, ".html"));

    if stat.graph_mode == "line" {
        add_line_graphs_on_plot(stat, &mut layout, &mut plot);
    } else if stat.graph_mode == "grouped_bar" {
        add_grouped_bar_on_plot(stat, &mut layout, &mut plot);
    } else if stat.graph_mode == "annotation" {
        add_annotation_graphs_on_plot(stat, &mut layout, &mut plot);
    } else {
        return;
    }

    for add_stat_name in &stat.add_stats {
        for stat in stats {
            if stat.name.eq(add_stat_name) {
                if stat.graph_mode == "line" {
                    add_line_graphs_on_plot(stat, &mut layout, &mut plot);
                } else if stat.graph_mode == "grouped_bar" {
                    add_grouped_bar_on_plot(stat, &mut layout, &mut plot);
                } else if stat.graph_mode == "annotation" {
                    add_annotation_graphs_on_plot(stat, &mut layout, &mut plot);
                }
            }
        }
    }


    plot.set_layout(layout);
    plot.use_local_plotly();
    plot.to_html(&graph_file_path);

    println!("Write graph html file at {}", graph_file_path.to_str().unwrap());
}

pub fn add_annotation_graphs_on_plot(stat: &Stat, layout: &mut Layout, plot: &mut Plot) {
    let path_output_file_stat = stat.get_output_file_path();
    println!("load data from {}", path_output_file_stat);

    let mut output_file_stat = csv::ReaderBuilder::new()
        .delimiter(b';')
        .flexible(true)
        .from_path(&path_output_file_stat)
        .expect(format!("Failed to open {}", path_output_file_stat).as_str());

    let mut at_x: Vec<String> = Vec::new();
    let mut at_y: Vec<f64> = Vec::new();
    for record in output_file_stat.records() {
        let record = record.unwrap();
        let x = String::from(&record[0]);
        at_x.push(x);
        at_y.push(1.);

        layout.add_shape(
            Shape::new()
                .shape_type(ShapeType::Line)
                .x_ref("x")
                .y_ref("paper")
                .x0(String::from(&record[0]))
                .y0(0)
                .x1(String::from(&record[0]))
                .y1(1)
                .line(ShapeLine::new().color(NamedColor::RoyalBlue).width(3.)),
        );
    }

    let trace = Scatter::new(at_x, at_y)
        .mode(Mode::Markers)
        .marker(
            Marker::new()
                .size(10)
                .line(Line::new().width(3.))
        )
        .name(&stat.name);
    plot.add_trace(trace);
}

pub fn add_line_graphs_on_plot(stat: &Stat, _layout: &mut Layout, plot: &mut Plot) {
    let path_output_file_stat = stat.get_output_file_path();
    println!("load data from {}", path_output_file_stat);

    let mut output_file_stat = csv::ReaderBuilder::new()
        .delimiter(b';')
        .flexible(true)
        .from_path(&path_output_file_stat)
        .expect(format!("Failed to open {}", path_output_file_stat).as_str());

    let headers = output_file_stat.headers().unwrap().clone();
    let headers_len: usize = headers.len();
    let mut values: Vec<Vec<f64>> = vec![Vec::new(); headers_len - 1];

    let mut at_x: Vec<String> = Vec::new();
    for record in output_file_stat.records() {
        let record = record.unwrap();
        at_x.push(String::from(&record[0]));
        for value_index in 1..headers_len {
            if &record.len() > &value_index {
                values[value_index - 1].push((&record[value_index]).parse().unwrap());
            } else {
                values[value_index - 1].push(0 as f64);
            }
        }
    }

    let mut traces: Vec<(String, Box<Scatter<String, f64>>)> = Vec::new();
    let values = values;
    for value_index in 1..headers_len {
        let at_x: Vec<String> = at_x.clone();
        let v: Vec<f64> = values[value_index - 1].clone();
        let trace_name = &headers[value_index];

        let trace = Scatter::new(at_x, v)
            .name(trace_name)
            .mode(Mode::Lines);

        traces.push((String::from(trace_name), trace));
    }

    traces.sort_by(|t1, t2| t1.0.cmp(&t2.0));

    for trace in traces {
        plot.add_trace(trace.1);
    }
}

pub fn add_grouped_bar_on_plot(stat: &Stat, _layout: &mut Layout, plot: &mut Plot) {
    let path_output_file_stat = stat.get_output_file_path();
    println!("load data from {}", path_output_file_stat);

    let mut output_file_stat = csv::ReaderBuilder::new()
        .delimiter(b';')
        .flexible(true)
        .has_headers(false)
        .from_path(&path_output_file_stat)
        .expect(format!("Failed to open {}", path_output_file_stat).as_str());

    let mut bar_def: HashMap<String, Vec<f64>> = HashMap::new();
    let mut at_x: Vec<String> = Vec::new();
    let mut start_values: Vec<f64> = Vec::new();

    for record in output_file_stat.records() {
        let record = record.unwrap();
        at_x.push(String::from(&record[0]));
        let mut values_by_bar: HashMap<String, f64> = HashMap::new();

        for i in 1..record.len() {
            let tmp2 = String::from(&record[i]);
            let p: Vec<&str> = tmp2.split("=").collect();
            let bar_name: String = String::from(p[0]);
            let value: f64 = p[1].parse().unwrap();
            values_by_bar.insert(bar_name, value);
        }

        for values_by_bar_entry in values_by_bar.iter() {
            match bar_def.get_mut(values_by_bar_entry.0) {
                Some(bar_values) => bar_values.push(*values_by_bar_entry.1),
                None => {
                    let mut bar_values = start_values.clone();
                    bar_values.push(*values_by_bar_entry.1);
                    bar_def.insert(values_by_bar_entry.0.as_str().parse().unwrap(), bar_values);
                }
            }
        }

        for bar_def_entry in bar_def.iter_mut() {
            if !values_by_bar.contains_key(bar_def_entry.0) {
                bar_def_entry.1.push(0.0);
            }
        }

        start_values.push(0.0);
    }


    let mut traces: Vec<(String, Box<Bar<String, f64>>)> = Vec::new();
    for bar_def_entry in bar_def.iter() {
        let trace = Bar::new(at_x.clone(), bar_def_entry.1.to_vec()).name(bar_def_entry.0);
        traces.push((String::from(bar_def_entry.0), trace));
    }

    traces.sort_by(|t1, t2| t1.0.cmp(&t2.0));

    for trace in traces {
        plot.add_trace(trace.1);
    }
}