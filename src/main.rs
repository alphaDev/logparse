use std::fs::File;
use std::io::{BufRead, BufReader, Write};

use clap::Parser;
use indicatif::{ProgressBar, ProgressStyle};
use linecount::count_lines;

use crate::args::Args;
use crate::graphs::make_graphs;
use crate::stats::Stat;

mod part;
mod formatter;
mod stats;
mod args;
mod graphs;


fn main() {
    let args: Args = Args::parse();
    println!("Hello, world!");

    let prepare = args.prepare;
    let mut stats: Vec<Stat> = Stat::build_from_args();
    for stat in &stats {
        stat.print();
    }

    if prepare {
        for stat in &stats {
            stat.do_prepare(args.prepare_count.unwrap_or(1));
        }
        return;
    }

    let log_file_path: &String = &args.log_to_parse;
    println!("start count line in file : {}", log_file_path);
    let log_file = File::open(log_file_path).expect(format!("Failed to open {}", log_file_path).as_str());
    let nb_line = count_lines(&log_file).expect("cannot read log file");
    println!("nb line in file  : {}", nb_line);

    for stat in &mut stats {
        stat.write_header();
    }

    let log_file_path: &String = &args.log_to_parse;
    let log_file2 = File::open(log_file_path).expect(format!("Failed to open {}", log_file_path).as_str());
    let log_file_buffer_reader = BufReader::new(log_file2);
    let pb = ProgressBar::new(nb_line as u64);
    let sty = ProgressStyle::default_bar()
        .template("[{elapsed_precise}] [{wide_bar}] {pos}/{len} ({eta}) {msg}")
        .progress_chars("#>-");
    let mut progress_step = nb_line / 100;
    if progress_step == 0 {
        progress_step = 1;
    }
    pb.set_style(sty);
    let mut line_index: usize = 0;
    for line in log_file_buffer_reader.lines() {
        let line = line.unwrap();
        line_index += 1;
        if line_index % progress_step == 0 {
            pb.inc(progress_step as u64);
        }

        for stat in &mut stats {
            stat.process_line(&line);
        }
    }

    for stat in &mut stats {
        match stat.output_buffer.as_mut() {
            Some(x) => x.flush(),
            None => Result::Ok(()),
        }.unwrap();
    }

    pb.finish_with_message("parsing done");


    make_graphs(&stats);

    println!("done");
}
