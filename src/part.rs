use regex::Captures;
use crate::formatter::Formatter;
use substring::Substring;

pub struct Part {
    pub raw: String,
    pub index: i64,
    pub raw_format: String,
    pub formatters: Vec<Formatter>,
}

impl Part {
    pub fn new(c: Captures) -> Part {
        return if !c.get(2).is_none() {
            let raw_formatters = &c[2];
            let formatters = Self::build_formatters(raw_formatters);

            Part { raw: String::from(&c[0]), index: c[1].parse::<i64>().unwrap(), raw_format: String::from(raw_formatters), formatters }
        } else {
            Part { raw: String::from(&c[0]), index: c[1].parse::<i64>().unwrap(), raw_format: String::new(), formatters: Vec::new() }
        };
    }

    pub fn build_formatters(raw_formatters: &str) -> Vec<Formatter> {
        let mut formatters: Vec<Formatter> = Vec::new();
        let raw_formatters_no_bracket = raw_formatters.substring(1, raw_formatters.len() - 1);

        for raw_formatter in raw_formatters_no_bracket.split(";") {
            let raw_formatter = raw_formatter.trim_end().trim_start();
            if raw_formatter != "" {
                formatters.push(Formatter::new(raw_formatter));
            }
        }
        return formatters;
    }

    pub fn print(&self) {
        let to_print = format!("raw 〖{}〗 index 〖{}〗 raw_format 〖{}〗", self.raw, self.index, self.raw_format);
        println!("Part[{}]", to_print);
        for f in &self.formatters {
            f.print();
        }
    }
}