use std::fs;
use std::fs::File;
use std::io::{BufRead, BufReader, BufWriter, Read, Write};
use std::path::Path;
use std::string::String;

use clap::Parser;
use cli_table::{Cell, CellStruct, print_stdout, Table};
use regex::Regex;
use yaml_rust::yaml::Array;
use yaml_rust::YamlLoader;

use crate::args::Args;
use crate::formatter::Formatter;
use crate::part::Part;

pub struct Stat {
    pub name: String,
    pub line_definition: String,
    pub line_definition_with_new_line: String,
    pub output_file: String,
    pub grep_pattern: String,
    pub header: String,
    pub parts: Vec<Part>,
    pub output_buffer: Option<BufWriter<File>>,
    pub log_to_parse: String,
    pub line_formatters: Vec<Formatter>,
    pub add_stats: Vec<String>,
    pub graph_mode: String,
    pub output_folder: String,
}

impl Stat {
    pub fn build_from_args() -> Vec<Stat> {
        let args: Args = Args::parse();

        return match args.file_definition {
            Some(file_definition) => {
                let mut file = File::open(file_definition.as_str()).expect("Unable to open file");
                let mut contents = String::new();
                file.read_to_string(&mut contents).expect("Unable to read file");

                let raw_definitions = YamlLoader::load_from_str(&contents).unwrap();
                let raw_stats: &Array = raw_definitions[0]["stats"].as_vec().unwrap();
                let mut stats: Vec<Stat> = Vec::new();
                let mut i = 0;
                let log_to_parse = args.log_to_parse;
                let log_to_parse_file_name = String::from(Path::new(&log_to_parse).file_stem().unwrap().to_str().unwrap());
                println!("log_to_parse_file_name {}", log_to_parse_file_name);

                for raw_definition in raw_stats {
                    let output_folder = log_to_parse_file_name.clone();
                    let line_definition = String::from(raw_definition["line_definition"].as_str().unwrap_or(""));
                    let parts = if line_definition.is_empty() { vec![] } else { Self::parse_part(line_definition.as_str()) };
                    let line_definition_with_new_line = format!("{}\n", line_definition);
                    let default_name = Path::new(log_to_parse.as_str()).file_name().unwrap().to_str().unwrap();
                    let name = String::from(raw_definition["name"].as_str().unwrap_or(format!("{}_stats_{}_parsed", i, default_name).as_str()));
                    let grep_pattern = String::from(raw_definition["grep_pattern"].as_str().unwrap_or(""));
                    let header = String::from(raw_definition["header"].as_str().unwrap_or(""));
                    let output_file: String = String::from(raw_definition["output_file"].as_str().unwrap_or(format!("{}.csv", name).as_str()));
                    let output_buffer: Option<BufWriter<File>> = if args.prepare { Option::None } else { Self::open_outfile_buffer(&output_folder, &output_file) };
                    let line_formatters: Vec<Formatter> = raw_definition["line_formatters"].as_vec().unwrap_or(Array::new().as_ref()).iter().map(|raw_formatter| Formatter::new(raw_formatter.as_str().unwrap_or(""))).collect();
                    let graph_mode: String = String::from(raw_definition["graph_mode"].as_str().unwrap_or("line"));
                    let add_stats: Vec<String> = raw_definition["add_stats"].as_vec().unwrap_or(Array::new().as_ref()).iter().map(|add_stat_name| String::from(add_stat_name.as_str().unwrap_or(""))).collect();
                    i += 1;


                    let stat = Stat {
                        output_file,
                        name,
                        line_definition,
                        line_definition_with_new_line,
                        grep_pattern,
                        header,
                        parts,
                        output_buffer,
                        log_to_parse: String::from(&log_to_parse),
                        line_formatters,
                        graph_mode,
                        add_stats,
                        output_folder,
                    };
                    stats.push(stat);
                }

                stats
            }
            None => {
                let log_to_parse = args.log_to_parse;
                let line_definition = args.line_definition.unwrap_or(String::new());
                let parts = if line_definition.is_empty() { vec![] } else { Self::parse_part(line_definition.as_str()) };
                let line_definition_with_new_line = format!("{}\n", line_definition);
                let default_name = Path::new(log_to_parse.as_str()).file_name().unwrap().to_str().unwrap();
                let name = args.name.unwrap_or(format!("{}_parsed", default_name));
                let grep_pattern = args.grep_pattern.unwrap_or(String::new());
                let header = args.header.unwrap_or(String::new());
                let output_file: String = args.output_file.unwrap_or(format!("{}.csv", name));
                let output_buffer: Option<BufWriter<File>> = if args.prepare { Option::None } else { Self::open_outfile_buffer(&log_to_parse, &output_file) };
                let line_formatters: Vec<Formatter> = Vec::new();
                let add_stats: Vec<String> = Vec::new();
                let graph_mode: String = args.graph_mode.unwrap_or(String::from("graph_mode"));
                let log_to_parse_file_name = String::from(Path::new(&log_to_parse).file_stem().unwrap().to_str().unwrap());

                vec![Stat {
                    output_file,
                    name,
                    line_definition,
                    line_definition_with_new_line,
                    grep_pattern,
                    header,
                    parts,
                    output_buffer,
                    log_to_parse,
                    line_formatters,
                    graph_mode,
                    add_stats,
                    output_folder: log_to_parse_file_name,
                }]
            }
        };
    }


    fn parse_part(line_def: &str) -> Vec<Part> {
        let regex = Regex::new(r"\$(-?[0-9]+)(\{[^}]*})*\$").unwrap();
        let cap = regex.captures_iter(line_def);


        let mut parts: Vec<Part> = Vec::new();

        for p in cap {
            let part: Part = Part::new(p);
            part.print();
            parts.push(part);
        }

        parts.sort_by_key(|k| k.index);
        parts
    }

    pub fn get_output_file_path(&self) -> String {
        let folder_path = Path::new(self.output_folder.as_str());
        let full_path = folder_path.join(self.output_file.as_str());
        let full_path2 = full_path.to_str().unwrap();
        return String::from(full_path2);
    }

    fn open_outfile_buffer(output_folder: &String, output_file_path: &String) -> Option<BufWriter<File>> {
        let folder_path = Path::new(output_folder.as_str());
        if !folder_path.exists() {
            fs::create_dir_all(output_folder.as_str()).err();
        }


        return if output_file_path.is_empty() {
            Option::None
        } else {
            let output_file = File::create(folder_path.join(output_file_path.as_str())).expect(format!("Failed to write file {}", output_file_path).as_str());
            Option::from(BufWriter::new(output_file))
        };
    }

    pub(crate) fn print(&self) {
        let to_print = format!("name 〖{}〗 grep_pattern 〖{}〗 line_definition 〖{}〗 header〖{}〗 output_file〖{}〗", self.name, self.grep_pattern, self.line_definition, self.header, self.output_file);
        println!("Stat[{}]", to_print);
    }

    pub(crate) fn write_header(&mut self) {
        if !self.header.is_empty() {
            let mut header_with_nl = String::from(&self.header);
            header_with_nl.push('\n');
            match self.output_buffer.as_mut() {
                Some(x) => x.write(header_with_nl.as_bytes()),
                None => Result::Ok(0),
            }.unwrap();
        }
    }

    pub(crate) fn process_line(&mut self, line: &String) {
        if line.contains(self.grep_pattern.as_str()) {
            let final_line = self.build_output_line(line);

            match self.output_buffer.as_mut() {
                Some(x) => x.write(final_line.as_bytes()),
                None => Result::Ok(0),
            }.unwrap();
        }
    }

    fn build_output_line(&self, line: &String) -> String {
        let mut value: String = String::new();

        let mut line = String::from(line);
        if !self.line_formatters.is_empty() {
            for formatter in &self.line_formatters {
                line = formatter.format(line);
            }
        }


        let split_line = line.split_whitespace().collect::<Vec<&str>>();
        let mut final_line = String::from(&self.line_definition_with_new_line);
        let count_part = split_line.len() as i64;

        for part in self.parts.iter() {
            let index_to_get: i64;
            if part.index < 0 {
                index_to_get = count_part + part.index;
            } else {
                index_to_get = part.index - 1;
            }

            if 0 <= index_to_get && index_to_get < count_part {
                let n = split_line.get(index_to_get as usize);
                if n.is_none() {
                    break;
                }
                value = String::from(*n.unwrap());
            }

            let mut final_value = value.clone();
            if !part.formatters.is_empty() {
                for formatter in &part.formatters {
                    final_value = formatter.format(final_value);
                }
            }

            final_line = final_line.replace(part.raw.as_str(), final_value.as_str());
        }
        if final_line.contains("$All$") {
            final_line = final_line.replace("$All$", &*line);
        }

        final_line
    }

    pub(crate) fn do_prepare(&self, prepare_count: usize) {
        println!("Doing the preparation");

        let log_file = File::open(&self.log_to_parse).expect(format!("Failed to open {}", &self.log_to_parse).as_str());
        let log_file_buffer_reader = BufReader::new(log_file);

        let nb_line_to_print = prepare_count;
        let mut nb_line_print = 0;
        for line in log_file_buffer_reader.lines() {
            let line = line.unwrap();

            if line.contains(self.grep_pattern.as_str()) {
                println!("line found");
                println!("----------------------");
                println!("{}", line);
                println!("----------------------");

                let split_line: Vec<CellStruct> = line.split_whitespace().map(|x| x.cell()).collect();
                let titles: Vec<CellStruct> = (1..split_line.len() + 1).map(|x| x.cell()).collect();
                let table = vec![split_line].table().title(titles);

                assert!(print_stdout(table).is_ok());

                if !self.line_definition.is_empty() {
                    println!("line definition");
                    println!("----------------------");
                    println!("{}", self.line_definition);
                    println!("----------------------");
                    println!("sample line");
                    println!("----------------------");
                    let final_line = self.build_output_line(&line);
                    println!("{}", final_line);
                    println!("----------------------");
                }
                nb_line_print += 1;
                if nb_line_print >= nb_line_to_print {
                    break;
                }
            }
        }
    }
}
